package com.yayitsandrew.heartbeat 
{

import flash.geom.Point;
import net.flashpunk.Entity;
import net.flashpunk.FP;
import net.flashpunk.Graphic;
import net.flashpunk.graphics.Text;
import net.flashpunk.Sfx;
import net.flashpunk.graphics.Image;
import net.flashpunk.utils.Ease;
import net.flashpunk.utils.Key;

public class Heart extends Entity 
{
	[Embed(source = "../../../../assets/heart.png")] private static const HEART : Class;
	[Embed(source = "../../../../assets/hit_left.png")] private static const HIT_LEFT : Class;
	[Embed(source = "../../../../assets/hit_right.png")] private static const HIT_RIGHT : Class;
	[Embed(source = "../../../../assets/heartbeat.mp3")] private static const HEARTBEAT_MP3 : Class;
	
	private const HEARTBEAT_MAX : int = 45;
	private const HEARTBEAT_MIN : int = 100;
	private const ERROR_HEARTBEAT_INTERVAL_MOD : int = 5;
	private const NATURAL_HEARTBEAT_INTERVAL_MOD : int = 2;
	private const NATURAL_BEATS_PER_INCREASE : int = 4;
	
	private var _isShaking : Boolean;
	private var _image : Image;
	private var _heartbeatFrame : int;
	private var _heartbeatFrameGoal : int;
	private var _heartbeatInterval : int;
	private var _heartSfx : Sfx;
	private var _hitZones : Vector.<Image>;
	private var _hitSpots : Vector.<Point>;
	private var _hitDir : int;
	private var _scoreMultiplier : int;
	private var _multiplierText : Text;
	private var _heartbeatCount : int;
	
	public function get hitDir() : int { return _hitDir; }
	
	public function Heart() 
	{
		super();
		
		_isShaking = false;
		_heartbeatInterval = HEARTBEAT_MIN;
		_heartbeatFrameGoal = _heartbeatInterval;
		_heartbeatFrame = _heartbeatFrameGoal;
		_scoreMultiplier = 1;
		_heartbeatCount = 0;
		
		_image = new Image( HEART );
		_image.centerOrigin();
		this.graphic = _image;
		
		_hitZones = new Vector.<Image>;
		_hitZones.push( new Image( HIT_LEFT ) );
		_hitZones.push( new Image( HIT_RIGHT ) );
		_hitZones.push( new Image( HIT_LEFT ) );
		_hitZones.push( new Image( HIT_RIGHT ) );
		
		_hitSpots = new Vector.<Point>;
		_hitSpots.push( new Point( 236, 140 ) );
		_hitSpots.push( new Point( 218, 140 ) );
		_hitSpots.push( new Point( 228, 164 ) );
		_hitSpots.push( new Point( 226, 164 ) );
		
		_heartSfx = new Sfx( HEARTBEAT_MP3 );
	}
	
	override public function added() : void 
	{
		x = FP.halfWidth + 8;
		y = FP.halfHeight -30;
		
		super.added();
		
		for ( var i : int = 0; i < _hitZones.length; i++ )
		{
			var hitZone : Image = _hitZones[ i ];
			
			hitZone.scale = 0.5;
			hitZone.visible = false;
			hitZone.x = _hitSpots[ i ].x;
			hitZone.y = _hitSpots[ i ].y;
			this.world.addGraphic( hitZone );
		}
		
		_multiplierText = new Text( "BAD" );
		_multiplierText.size = 10;
		_multiplierText.visible = false;
		this.world.addGraphic( _multiplierText );
	}
	
	override public function update() : void 
	{
		if ( ( this.world as HeartbeatWorld ).isGameOver )
		{
			return;
		}
		
		_heartbeatFrame += 1;
		if ( _heartbeatFrame >= _heartbeatFrameGoal )
		{
			_heartbeatFrame = 0;
			_heartbeatFrameGoal = _heartbeatInterval;
			_heartSfx.play();
			
			_heartbeatCount += 1;
			if ( _heartbeatCount >= NATURAL_BEATS_PER_INCREASE )
			{
				increaseHeartbeat();
				_heartbeatCount = 0;
			}
			
			var dirs : Array = [ Key.UP, Key.DOWN, Key.LEFT, Key.RIGHT ];
			var idx : int = Math.random() * dirs.length;
			_hitDir = dirs[ idx ];
			
			for ( var i : int = 0; i < _hitZones.length; i++ )
			{
				if ( i == idx ) //fade in
				{
					if ( ! _hitZones[ i ].visible )
					{
						_scoreMultiplier = 1;
						
						_hitZones[ i ].alpha = 0.0;
						_hitZones[ i ].visible = true;
						FP.tween( _hitZones[ i ], { alpha : 1.0 }, 0.4, { ease : Ease.backOut } );
					}
				}
				else //fade out
				{
					FP.tween( _hitZones[ i ], { alpha : 0.3 }, 0.1, { ease : Ease.backIn,
						complete : Delegate.create( onFadeOutComplete, _hitZones[ i ] ) } );
				}
			}
		}
		
		super.update();
	}
	
	private function onFadeOutComplete( a_graphic : Graphic ) : void
	{
		a_graphic.visible = false;
	}
	
	//the game slowly increases the heart rate through the course of the game
	public function increaseHeartbeat() : void
	{
		//don't go above the kill speed by natural increases
		if ( _heartbeatInterval - NATURAL_HEARTBEAT_INTERVAL_MOD <= HEARTBEAT_MAX )
		{
			return;
		}
		
		var newInterval : int = Math.max( HEARTBEAT_MAX + 1, _heartbeatInterval - NATURAL_HEARTBEAT_INTERVAL_MOD );
		if ( newInterval != _heartbeatInterval )
		{
			_heartbeatInterval = newInterval;
			_heartbeatFrameGoal -= NATURAL_HEARTBEAT_INTERVAL_MOD;
		}
	}
	
	//missing will rapidly accelerate the heartrate
	public function accelerateHeartbeat() : void
	{
		if ( _heartbeatInterval - ERROR_HEARTBEAT_INTERVAL_MOD < HEARTBEAT_MAX )
		{
			( this.world as HeartbeatWorld ).gameOver();
		}
		
		var newInterval : int = Math.max( HEARTBEAT_MAX, _heartbeatInterval - ERROR_HEARTBEAT_INTERVAL_MOD );
		if ( newInterval != _heartbeatInterval )
		{
			_heartbeatInterval = newInterval;
			_heartbeatFrameGoal -= ERROR_HEARTBEAT_INTERVAL_MOD;
		}
		
		showMultiplierText( "BAD" );
		
		var delay : Number = 0.05;
		FP.tween( FP.screen, { color : 0x660000 }, delay );
		FP.tween( FP.screen, { color : 0x000000 }, delay, { delay : delay } );
		FP.tween( FP.screen, { color : 0x660000 }, delay, { delay : delay * 2 } );
		FP.tween( FP.screen, { color : 0x000000 }, delay, { delay : delay * 3 } );
	}
	
	public function shake() : void
	{
		shakeWithForce( 3 );
	}
	
	public function shakeHard() : void
	{
		shakeWithForce( 5 );
	}
	
	private function shakeWithForce( a_force : int ) : void
	{
		if ( _isShaking )
		{
			return;
		}
		
		_isShaking = true;
		
		if ( _scoreMultiplier > 1 )
		{
			showMultiplierText( "x" + String( _scoreMultiplier ) );
		}
		else
		{
			showMultiplierText( "OK" );
		}
		
		( this.world as HeartbeatWorld ).updateScore( 1 * _scoreMultiplier );
		_scoreMultiplier += 1;
		
		var length : Number = 0.05;
		var startX : int = _image.x;
		
		FP.tween( _image, { x : startX - a_force }, length );
		FP.tween( _image, { x : startX + a_force }, length, { delay : length } );
		FP.tween( _image, { x : startX - a_force }, length, { delay : length * 2 } );
		FP.tween( _image, { x : startX }, length, { delay : length * 3, complete : onShakeComplete } );
	}
	
	private function showMultiplierText( a_text : String ) : void
	{
		_multiplierText.text = a_text;
		
		var hitZone : Image = getActiveHitZone();
		_multiplierText.x = hitZone.x;
		_multiplierText.y = hitZone.y;
		_multiplierText.visible = true;
		FP.tween( _multiplierText, { y : _multiplierText.y - 40 }, 0.3,
			{ ease : Ease.backOut, complete : Delegate.create( onFadeOutComplete, _multiplierText ) } );
	}
	
	private function getActiveHitZone() : Image
	{
		for each( var hitZone : Image in _hitZones )
		{
			if ( hitZone.visible )
			{
				return hitZone;
			}
		}
		
		trace( "ERROR, TRIED TO GET ACTIVE HIT ZONE AND NULLED" );
		return null;
	}
	
	private function onShakeComplete() : void
	{
		_isShaking = false;
	}
}

}