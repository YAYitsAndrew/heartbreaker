package com.yayitsandrew.heartbeat 
{

import flash.geom.Rectangle;
import net.flashpunk.FP;
import net.flashpunk.Sfx;
import net.flashpunk.World;
import net.flashpunk.graphics.Canvas;
import net.flashpunk.graphics.Image;
import net.flashpunk.graphics.Text;
import net.flashpunk.utils.Input;

public class HeartbeatWorld extends World 
{
	[Embed(source = "../../../../assets/controls.png")] private static const CONTROLS : Class;
	[Embed(source = "../../../../assets/heartdeath.mp3")] private static const HEARTDEATH_MP3 : Class;
	
	private var _mac : Mac;
	private var _heart : Heart;
	private var _score : Text;
	private var _isGameOver : Boolean;
	private var _heartdeath : Sfx;
	
	public function get heart() : Heart { return _heart; }
	public function get isGameOver() : Boolean { return _isGameOver; }
	
	public function HeartbeatWorld() 
	{
		super();
		
		_isGameOver = false;
	}
	
	override public function begin() : void 
	{
		var controls : Image = new Image( CONTROLS );
		controls.x = 4;
		controls.y = FP.height - controls.height - 4;
		this.addGraphic( controls );
		
		_heart = new Heart();
		this.add( _heart );
		
		_mac = new Mac();
		this.add( _mac );
		
		_heartdeath = new Sfx( HEARTDEATH_MP3 );
		
		_score = new Text( "00" );
		_score.align = "right";
		_score.size = 48;
		_score.x = 4;
		_score.y = 4;
		this.addGraphic( _score, -1 );
		
		super.begin();
	}
	
	override public function update() : void 
	{
		super.update();
		
		if ( _isGameOver && Input.mousePressed )
		{
			_heartdeath.stop();
			FP.world = new StartWorld();
		}
	}
	
	public function gameOver() : void
	{
		_isGameOver = true;
		
		_heartdeath.play();
		
		var canvas : Canvas = new Canvas( FP.width, FP.height );
		canvas.fill( new Rectangle( 0, 0, FP.width, FP.height ), 0x660000 );
		this.addGraphic( canvas );
		canvas.alpha = 0.0;
		
		FP.tween( _heart.graphic, { alpha : 0.9, scale : 6.0 }, 0.1 );
		FP.tween( canvas, { alpha : 0.9 }, 0.2 );
		
		var gameoverText : Text = new Text( "Game Over" );
		gameoverText.size = 36;
		gameoverText.x = ( FP.width - gameoverText.width ) / 2;
		gameoverText.y = ( FP.height - gameoverText.height ) / 2;
		this.addGraphic( gameoverText );
	}
	
	public function updateScore( a_points : int ) : void
	{
		var score : int = int( _score.text ) + a_points;
		_score.text = ( score < 10 ) ? "0" + String( score ) : String( score );
	}
}

}