package com.yayitsandrew.heartbeat
{

import net.flashpunk.Engine;
import net.flashpunk.FP;
import net.flashpunk.graphics.Text;

public class Main extends Engine
{
	[Embed(source = "../../../../assets/PressStart2P.ttf", embedAsCFF = "false", fontFamily = "PIXEL_FONT")]
		private static const PIXEL_FONT : Class;
	
	public function Main() : void
	{
		super( 480, 320 );
		
		FP.screen.color = 0x000000;
		Text.font = "PIXEL_FONT";
	}
	
	override public function init():void
	{
		super.init();
		FP.world = new StartWorld();
	}
}

}