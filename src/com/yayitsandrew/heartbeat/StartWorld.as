package com.yayitsandrew.heartbeat 
{

import flash.geom.Rectangle;
import net.flashpunk.FP;
import net.flashpunk.World;
import net.flashpunk.graphics.Canvas;
import net.flashpunk.graphics.Text;
import net.flashpunk.utils.Input;

public class StartWorld extends World 
{
	public function StartWorld() 
	{
		super();
	}
	
	override public function begin():void 
	{
		var canvas : Canvas = new Canvas( FP.width, FP.height );
		canvas.fill( new Rectangle( 0, 0, FP.width, FP.height ), 0x660000 );
		this.addGraphic( canvas );
		
		var startText : Text = new Text( "Click to Start" );
		startText.size = 24;
		startText.x = ( FP.width - startText.width ) / 2;
		startText.y = ( FP.height - startText.height ) / 2;
		this.addGraphic( startText );
		
		super.begin();
	}
	
	override public function update() : void 
	{
		super.update();
		
		if ( Input.mousePressed )
		{
			FP.world = new HeartbeatWorld();
		}
	}
}

}