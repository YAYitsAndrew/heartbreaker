package com.yayitsandrew.heartbeat 
{

import net.flashpunk.Entity;
import net.flashpunk.FP;
import net.flashpunk.graphics.Spritemap;
import net.flashpunk.utils.Input;
import net.flashpunk.utils.Key;

public class Mac extends Entity 
{
	[Embed(source="../../../../assets/mac.png")] private static const MAC : Class;
	
	private var _sprite : Spritemap;
	private var _attackMode : int;
	
	public function Mac()
	{
		super();
		
		_attackMode = Key.DOWN;
		
		_sprite = new Spritemap( MAC, 56, 162 );
		_sprite.add( "idle", [ 0, 1 ], 3 );
		_sprite.add( "punch", [ 2, 3, 4, 4, 4 ], 16, false );
		_sprite.add( "highpunch", [ 5, 6, 7, 7, 7 ], 16, false );
		_sprite.callback = onAnimFinished;
		_sprite.play( "idle" );
		
		this.graphic = _sprite;
	}
	
	override public function added() : void
	{
		x = ( FP.width - _sprite.width ) / 2;
		y = FP.height - _sprite.height - 10;
		
		super.added();
	}
	
	override public function update() : void
	{
		if ( ( this.world as HeartbeatWorld ).isGameOver )
		{
			return;
		}
		
		if ( _sprite.currentAnim != "idle" )
		{
			return;
		}
		
		var world : HeartbeatWorld = ( this.world as HeartbeatWorld );
		world.bringToFront( this );
		
		if ( Input.pressed( Key.UP ) )
		{
			_attackMode = Key.UP;
		}
		else if ( Input.pressed( Key.DOWN ) )
		{
			_attackMode = Key.DOWN;
		}
		
		var didHit : Boolean = false;
		var inputGiven : Boolean = false;
		
		if ( Input.pressed( Key.LEFT ) )
		{
			inputGiven = true;
			
			if ( _attackMode == Key.UP )
			{
				_sprite.play( "highpunch" );
				didHit = ( world.heart.hitDir == Key.UP );
			}
			else
			{
				_sprite.play( "punch" );
				didHit = ( world.heart.hitDir == Key.LEFT );
			}
		}
		else if ( Input.pressed( Key.RIGHT ) )
		{
			inputGiven = true;
			_sprite.flipped = true;
			
			if ( _attackMode == Key.UP )
			{
				_sprite.play( "highpunch" );
				didHit = ( world.heart.hitDir == Key.DOWN );
			}
			else
			{
				_sprite.play( "punch" );
				didHit = ( world.heart.hitDir == Key.RIGHT );
			}
		}
		
		if ( inputGiven )
		{
			if ( didHit )
			{
				world.heart.shake();
			}
			else
			{
				world.heart.accelerateHeartbeat();
			}
		}
		
		super.update();
	}
	
	private function onAnimFinished() : void
	{
		if ( _sprite.currentAnim == "punch" || _sprite.currentAnim == "highpunch" )
		{
			if ( _sprite.flipped )
			{
				_sprite.flipped = false;
			}
			_sprite.play( "idle" );
		}
	}
}

}